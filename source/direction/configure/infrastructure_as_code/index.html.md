---
layout: markdown_page
title: "Category Vision - Infrastructure as Code"
---

- TOC
{:toc}

## Infrastructure as Code

Infrastructure as code (IaC) is the practice of managing and provisioning infrastructure through machine-readable definition files, rather than physical hardware configuration or interactive configuration tools. The IT infrastructure managed by this comprises both physical equipment such as bare-metal servers as well as virtual machines and associated configuration resources. The definitions are stored in a version control system. IaC takes proven coding techniques and extends them to your infrastructure directly, effectively blurring the line between what is an application and what is the environment.

Our focus will be to provide tight integration with best of breed IaC tools, such that all infrastructure related workflows in GitLab are well supported. Our initial focus will be on Terraform.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Category%3AInfrastructure%20as%20Code)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1925) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

#### What's Next & Why
[Define Kubernetes cluster base domain via API](https://gitlab.com/gitlab-org/gitlab/issues/32360)

We want to empower operators to carry out all their infrastructure-related needs in a programatic way. To that end, once a cluster is created, the must be able to configure its attributes via IaC.

#### What is Not Planned Right Now
Integration with tools other than Terraform is not currently planned.

#### Maturity Plan
The category is `Minimal` at the moment. In order to get to `Viable` we must be able to not only provision Kubernetes resources but configure them as well. This involves configuration of the cluster and installation of helm charts.

### Analyst Landscape
We don't consider GitLab a replacement of IaC tools, rather a complement.

### Top Customer Success/Sales issue(s)
TBD

### Top user issue(s)
TBD

### Top internal customer issue(s)
[Define Kubernetes cluster base domain via API](https://gitlab.com/gitlab-org/gitlab/issues/32360)

### Top Strategy Item(s)
[Define Kubernetes cluster base domain via API](https://gitlab.com/gitlab-org/gitlab/issues/32360)

### Examples

- [how GitLab uses Terraform](https://about.gitlab.com/blog/2019/11/12/gitops-part-2/) internally
- [Kiwi.com on Infrastructure as Code](https://www.youtube.com/watch?v=Un2mJrRFSm4) at GitLab Commit London, 2019
- presenting [code.siemens.com](https://www.youtube.com/watch?v=4Y8zv1TJRlM) at GitLab Commit London, 2019