---
title: "AWS Lambda usage survey results"
author: Viktor Nagy # if name includes special characters use double quotes "First Last"
author_gitlab: nagyv.gitlab # ex: johndoe
author_twitter: nagyviktor # ex: johndoe
categories: unfiltered
image_title: "/images/blogimages/gitlab-values-cover.png"
description: "The results of our quick AWS Lambda usage survey"
tags: developer survey, integrations
cta_button_text: 'Start an AWS Lambda project with GitLab' # optional
cta_button_link: 'https://gitlab.com/projects/new' # optional
guest: false # required when the author is not a GitLab Team Member
ee_cta: false # required only if you do not want to display the EE-trial banner
install_cta: false # required only if you do not want to display the 'Install GitLab' banner
postType: content marketing # i.e.: content marketing, product, corporate
---

{::options parse_block_html="true" /}

<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>&nbsp;&nbsp;
This blog post is [Unfiltered](/handbook/marketing/blog/unfiltered)
&nbsp;&nbsp;<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{: .alert .alert-webcast}

<!-- Content start here -->

In early October, I asked the community to [share your AWS Lambda tooling habits](https://forms.gle/9xhjaPxKdZsHDs2V9), so we can better serve your needs from within GitLab. This blog post presents the results of that survey.

## Intro

So, what did I ask you about? I had a few assumptions in mind when I put together the survey.

- Lambda is mostly used by developers, but - at least in the enterprise - ops people might be involved too, because of monitoring or security.
- There are differences between hobby and professional usage, and I wanted to be able to filter out hobby users.
- Serverless has an adoption path in the enterprise. This might result in it being used only for backoffice scripts at first. So I wanted to know if it's used in production or backoffice scripting only.

Besides testing these assumptions, I wanted to learn about usage habits with respect to:

- Frameworks used (if any)
- Testing tools and approaches used
- CI/CD tools used
- Monitoring and debugging tools and approaches followed

The survey was shared on Reddit, some Facebook Groups, and on the GitLab Twitter and Facebook channels. As a result, we received 58 responses.

Who answered the survey?
{: .note.text-center}

![User role](/images/blogimages/aws-lambda-survey-2019/aws-population.png)

## How do we write code for AWS Lambda

The first interesting topic was what frameworks were being used for AWS Lambda. It was possible to select multiple options and responders could even provide a free text answer.

![Company size](/images/blogimages/aws-lambda-survey-2019/aws-frameworks.png){: .small.left.wrap-text}

As many responders chose more than just a single option, we should get a bit behind the data to find real insights. From that you can see that the Serverless framework rules the market and is really wide-spread. The *Other* section is quite scattered, there are no other big players hidden behind (it includes Zappa, Chalice, Netlify function, etc). 

I thought that there might be differences once I controled for the company size. I expected more Terraform and less CLI usage as the company size increases. I didn't look into statistical significance, but a simple eye-ball test shows that SMBs are going heads down with tech stacks, they are the strongest users of both Serverless and Terraform. I'd say that enterprise users try to follow along, but have a quite big direct AWS CLI usage too. Why might this be true? A few scenarios come to mind that can be validated/falsified:

- Enterprises are lagging in terms of technology adoption, thus their Terraform usage is lower
- They use AWS CLI more extensively as the Serverless framework can't fulfill all their use cases
- Possibly, we don't have enough data, and with a stronger analysis it would turn out that there are no differences

## What about testing

When asked about the challenges that serverless technologies pose one topic repeatedly arose: the lack of good testing infrastructure.

At GitLab we strive to provide outstanding CI/CD capabilities for testing. We also work hard to spread best practices. 

I asked the community about the current approaches they take to CI/CD and testing. Here again, multiple answers were allowed.

![Company size](/images/blogimages/aws-lambda-survey-2019/aws-testing.png){: .small.right.wrap-text}

This pie chart is filtered to show only non-hobby projects. Even here, almost every 5th project has no testing at all! Otherwise, we can barely speak about test paradigms here as the majority of the projects either don't run any tests or run only unit tests.

Getting into the data by company size, we see what we would expect: as the company size grows, testing becomes more important.

## CI/CD bias

The survey also contained a question about which CI/CD tools are being used. I skipped the analysis here. As the survey was mostly shared by GitLab team members, and through GitLab channels, clearly the majority of responders use GitLab. A wise choice! :)

## Monitoring

Alongside developing and deploying software, thinking about its operational health in production is just as important. This led me to ask a few questions on Lambda monitoring habits.

![Company size](/images/blogimages/aws-lambda-survey-2019/aws-monitoring.png){: .small.left.wrap-text}

I was surprised by the vast majority primarily just going with AWS CloudWatch. I expected that most production environments would use more advanced instrumentation, I was wrong.

A related question I asked is, "What metrics are you  most interested in?" This was a free-text answer. There were no surprises
here with "error rates" coming out as the clear winner.

## Conclusion

Based on the survey it became clear that even today, GitLab can be used very well with AWS lambda. To make getting started
easy, we've created a project template that uses GitLab Pages to host the frontend of your app, and AWS Lambda for your backend
needs. Besides the basic hosting needs, our templates has `serverless-offline` support added, so you can start writing tests against it without any additional setup needed. You can easily get started by starting a [new project using the Serverless Framework/JS template](https://gitlab.com/projects/new)

![Easy getting started with project templates](/images/blogimages/aws-lambda-survey-2019/aws-project-template.png){: .medium}

These were the insights I gathered about the data. Because this data was provided by the community, I'm making it available to everyone. You can [download the responses as a csv](/images/blogimages/aws-lambda-survey-2019/aws-lambda-survey-responses.csv). In case you are serious about Serverless usage in production, I'd love to hear your insights! Feel free to [reach out to me](https://gitlab.com/nagyv.gitlab)!

<!-- Content ends here -->

DISCLAIMER: This blog is intended for user-generated content submitted by the GitLab team. The views and opinions represented in this blog are personal to the author of each respective blog post and do not represent the views or opinions of GitLab unless explicitly stated.
All content provided on this blog is for informational purposes only. Neither GitLab nor any of the individual blog contributors ("Contributors") make any representations as to the accuracy or completeness of any information on this site. Neither GitLab nor any Contributors will be liable for any errors or omissions in this information or any losses, injuries, or damages from the display or use of this information.
Comments are welcome, and in fact, encouraged. However, GitLab reserves the right to edit or delete any comments submitted to this blog without notice should GitLab determine them to i) be spam or questionable spam; ii) include profanity; iii) include language or concepts that could be deemed offensive, hate speech, credible threats, or direct attacks on an individual or group; or iv) are in any other way a violation of GitLab's Website Terms of Use. GitLab is not responsible for the content in comments.
This policy is subject to change at any time.
{: .alert .alert-info .note}
