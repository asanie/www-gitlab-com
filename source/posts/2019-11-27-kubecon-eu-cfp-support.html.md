---
title: "KubeCon EU CFP Support"
author: Emily Chin
author_gitlab: echin
author_twitter: emilybchin
categories: unfiltered
image_title: "/images/blogimages/gitlab-values-cover.png"
description: "GitLab's Tech Evangelism team wants to help you get on stage at KubeCon EU!"
tags: tag1, tag2, tag3
guest: false # required when the author is not a GitLab Team Member
ee_cta: false # required only if you do not want to display the EE-trial banner
install_cta: false # required only if you do not want to display the 'Install GitLab' banner
twitter_text: "GitLab wants to help you get on stage at KubeCon EU!" # optional;  If no text is provided it will use post's title.
featured: yes # reviewer should set
postType: content definition # i.e.: content marketing, product, corporate
merch_banner_destination_url: ""
merch_banner_image_source: "/images/merchandising-content/XXX.jpg"
merch_banner_body_title: ""
merch_banner_body_content: ""
merch_banner_cta_text: ""
merch_sidebar_destination_url: ""
merch_sidebar_image_source: "/images/merchandising-content/XXX.jpg"
merch_sidebar_body_title: ""
merch_sidebar_body_content: ""
merch_sidebar_cta_text: ""
---

{::options parse_block_html="true" /}

<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>&nbsp;&nbsp;
This blog post is [Unfiltered](/handbook/marketing/blog/unfiltered)
&nbsp;&nbsp;<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{: .alert .alert-webcast}

It’s that time of year: the deadline for proposals to the [2020 KubeCon/ CloudNativeCon Europe](https://events19.linuxfoundation.org/events/kubecon-cloudnativecon-europe-2020/) is coming up fast. Not to worry though; GitLab’s [Technical Evangelism](https://about.gitlab.com/handbook/marketing/technical-evangelism/) team is here to help. 

I’m new to the Cloud Native community but, after attending my very first KubeCon this week in my backyard of [San Diego](https://events19.linuxfoundation.org/events/kubecon-cloudnativecon-north-america-2019/), I couldn’t be more excited to dive in. Listening in on the amazing talks from the cloud native community was energizing, inspiring, and illuminating, and it’s awesome that GitLab wants to empower our community to share their ideas and learnings too. Our team is ready to review your proposals and flesh out any ideas that folks want to submit to the [open call for proposals](https://linuxfoundation.smapply.io/prog/kccnceu2020/). Let us know how we can help today: the CFP closes on Wednesday, December 4, 2019 at 11:59PM PDT. 

GitLab’s community is passionate, vocal, and deeply engaged, and elevating the voices of our users. If we can support with your submissions, feel free to complete [this form](https://forms.gle/W1EWHKVjptjBLSSB7) to get the ideas flowing, or reach out to our Director of Technical Evangelism directly on Twitter – [@pritianka](https://twitter.com/pritianka).

The call for proposals for GitLab KubeCon Amsterdam closes on December 4! [Get more details and submit your talks here](https://events19.linuxfoundation.org/events/kubecon-cloudnativecon-europe-2020/call-for-proposals/).

DISCLAIMER: This blog is intended for user-generated content submitted by the GitLab team. The views and opinions represented in this blog are personal to the author of each respective blog post and do not represent the views or opinions of GitLab unless explicitly stated.
All content provided on this blog is for informational purposes only. Neither GitLab nor any of the individual blog contributors ("Contributors") make any representations as to the accuracy or completeness of any information on this site. Neither GitLab nor any Contributors will be liable for any errors or omissions in this information or any losses, injuries, or damages from the display or use of this information.
Comments are welcome, and in fact, encouraged. However, GitLab reserves the right to edit or delete any comments submitted to this blog without notice should GitLab determine them to i) be spam or questionable spam; ii) include profanity; iii) include language or concepts that could be deemed offensive, hate speech, credible threats, or direct attacks on an individual or group; or iv) are in any other way a violation of GitLab's Website Terms of Use. GitLab is not responsible for the content in comments.
This policy is subject to change at any time.
{: .alert .alert-info .note}
